from datetime import datetime

import discord

# import commands
import config
from utils import utils
import requests
import json

last_state = {}


async def check_online():
    if (datetime.now().hour >= 3) and (datetime.now().hour <= 5):
        return
    state = {}
    stopped = []
    started = []
    for api in config.API_keys_pterodactyl:  # This for-loop is for providing multiple api keys and or api urls
        for server in api["servers"]:  # get state of every server belonging to the api key of this loop-run
            data = await utils.call_pterodactyl_api(api, server)
            if data:
                state[server["name"]] = data["attributes"]["current_state"]

    for server in state:
        if server in last_state:
            if state[server] != last_state[server]:
                if state[server] == "offline":
                    stopped.append(server)

                elif state[server] == "running":
                    started.append(server)

                last_state[server] = state[server]
        else:
            if state[server] != "running":
                stopped.append(server)

            last_state[server] = state[server]
    if stopped or started:
        channel = await config.bot.fetch_channel(config.Watchdog_channel_id)
        message = "@here\n"
        if stopped:
            if len(stopped) == 1:
                message += "Der Server "
            else:
                message += "Die Server "
            message += ", ".join(stopped)
            if len(stopped) == 1:
                message += " läuft nicht mehr!\n"
            else:
                message += " laufen nicht mehr!\n"
        if started:
            if len(started) == 1:
                message += "Der Server "
            else:
                message += "Die Server "
            message += ", ".join(started)
            if len(started) == 1:
                message += " läuft wieder!\n"
            else:
                message += " laufen wieder!\n"

        await channel.send(message)
