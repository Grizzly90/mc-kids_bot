import discord
from discord.ext import commands

from platformroles import platformroles

# eigene Bot-Klasse, um persistente Views zu ermöglichen
class Alfred(commands.Bot):
    def __init__(self):
        intents = discord.Intents().all()
        super().__init__(command_prefix=commands.when_mentioned_or('~'), intents=intents)
    # wird in on_ready aufgerufen    
    def initialize(self):
        print('Initializing Alfred ...')
        self.add_view(platformroles.get_view())
