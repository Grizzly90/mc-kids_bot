import asyncio
from signal import SIGINT, SIGTERM
import datetime
import logging
import sys
from datetime import datetime, timedelta

from dotenv import load_dotenv
from watchdog import watchdog
import sqlite3
import discord
from discord.ext import commands, tasks

import config
from commands import commands
import events.events as events
from webserver import webserver
from utils import utils
from ticketsystem import ticketsystem

# load_dotenv()

if not config.dbExists:
    # config.Connection = sqlite3.connect("data/bot.sqlite")
    config.Cursor = config.Connection.cursor()
    config.Cursor.execute("CREATE TABLE variables("
                          "variable TEXT PRIMARY KEY, "
                          "channel_id INTEGER)")
    config.Cursor.execute("CREATE TABLE tickets("
                          "ticket_id INTEGER PRIMARY KEY, "
                          "channel_id INTEGER, "
                          "creator INTEGER, "
                          "assigned INTEGER, "
                          "status TEXT, "
                          "admin_mode INTEGER, "
                          "close_reason TEXT, "
                          "reopen_reason TEXT)")
    config.Cursor.execute("CREATE TABLE todo("
                          "ticket_id INTEGER, "
                          "task TEXT, "
                          "done INTEGER)")
    config.Cursor.execute("CREATE TABLE additional_users("
                          "ticket_id INTEGER, "
                          "user INTEGER)")
    config.Connection.commit()


# ------- Define Tasks -----
@tasks.loop(minutes=5.0)
async def check_ticket():
    for x in config.last_opened_ticket:
        if datetime.now() > x[1] + timedelta(minutes=15):
            channel = await config.bot.fetch_channel(x[2])
            if channel:
                await ticketsystem.delete_ticket(channel)
            config.last_opened_ticket.remove([x[0], x[1], x[2]])


@check_ticket.before_loop
async def before_check_ticket():
    await config.bot.wait_until_ready()


@tasks.loop(minutes=1.0)
async def check_rule_messages():
    for x in config.Rules_messages:
        if datetime.now() > x[1] + timedelta(minutes=1):
            config.Rules_messages.remove(x)
            await x[0].delete()
            await x[2].delete()


@check_rule_messages.before_loop
async def before_check_rule_messages():
    await config.bot.wait_until_ready()


@tasks.loop(minutes=1.0)
async def check_server_state():
    await watchdog.check_online()


@check_server_state.before_loop
async def before_server_state():
    await config.bot.wait_until_ready()
    # await asyncio.sleep(1)


@config.bot.event
async def on_voice_state_update(member, before, after):
    if member == config.bot.user:
        return

    if after.channel is not None:
        if after.channel.id == 752611871915245580:
            config.vc = await member.voice.channel.connect()
            audio_source = discord.FFmpegPCMAudio(source='test.mp3')
            config.vc.play(audio_source)
            while config.vc.is_playing():
                await asyncio.sleep(1)
                # disconnect after the player has finished
            config.vc.stop()
            await config.vc.disconnect()

    if before.channel is not None:
        if before.channel.id == 752611871915245580:
            if len(before.channel.members) == 1 and before.channel.members[0].id is config.bot.user.id:
                for vc in config.bot.voice_clients:
                    if vc.guild.id == config.Guild_id:
                        await vc.disconnect()


# ------ Start tasks -----------
async def main():
    await webserver.webserver()
    async with config.bot:
        await config.bot.start(config.token)
        await check_rule_messages.start()
        await check_server_state.start()
        await check_ticket.start()

asyncio.run(main())
