import asyncio, threading
from datetime import datetime

import discord

# import commands
import config
from utils import utils

newTicketLock = threading.Lock()

async def do_ticket_reaction(payload):
    global newTicketLock
    user = payload.member
    channel = await config.bot.fetch_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)
    if payload.message_id == config.Open_ticket_message.id:
        if payload.emoji.name == config.Open_ticket_reaction:  # right reaction; Create Channel
            if newTicketLock.acquire(timeout=30):
                try:
                    await message.remove_reaction(payload.emoji, user)
                    for x in config.last_opened_ticket:
                        if x[0] == user.id:
                            await user.send("Du hast aktuell schon ein Ticket geöffnet. "
                                            "Bitte stelle dort alle Infos bereit, bevor du ein neues öffnest.")
                            return
                    config.Ticket_id += 1
                    overwrites = {
                        config.Guild.default_role: discord.PermissionOverwrite.from_pair(discord.Permissions.none(),
                                                                                         discord.Permissions.all_channel()),
                        config.Guild.me: discord.PermissionOverwrite.from_pair(discord.Permissions.all_channel(),
                                                                               discord.Permissions.none()),
                        config.Ticket_admin_role: discord.PermissionOverwrite.from_pair(discord.Permissions.all_channel(),
                                                                                        discord.Permissions.none()),
                        config.Ticket_mod_role: discord.PermissionOverwrite.from_pair(discord.Permissions.text(),
                                                                                      discord.Permissions.none()),
                        user: discord.PermissionOverwrite.from_pair(discord.Permissions(config.Private_allow_permissions),
                                                                    discord.Permissions(config.Private_deny_permissions))
                    }
                    title = 'Neues Ticket von ' + user.name
                    ticket_channel = await config.Guild.create_text_channel(title,
                                                                            category=config.Ticket_category,
                                                                            overwrites=overwrites)

                    config.Cursor.execute("INSERT INTO tickets VALUES(?, ?, ?, -1, 'open', 0 , '', '');", [config.Ticket_id,
                                                                                                           ticket_channel.id,
                                                                                                           user.id])
                    config.Cursor.execute("REPLACE INTO variables VALUES('Ticket_id', ?);", [config.Ticket_id])
                    await send_ticket_opened_message(ticket_channel)
                    await ticket_channel.send("Hallo {.mention},\nBitte Beschreibe dein Anliegen".format(user))
                    config.last_opened_ticket.append([user.id, datetime.now(), ticket_channel.id])
                    config.Connection.commit()
                finally:
                    newTicketLock.release()
            else:
                await user.send("Die Ticket-Datenbank ist gesperrt. Bitte melde dies als Fehler.")
        else:  # wrong reaction; delete reaction
            await message.remove_reaction(payload.emoji, user)

    else:
        config.Cursor.execute("SELECT * FROM tickets WHERE channel_id = ?;", [channel.id])
        data = config.Cursor.fetchone()
        if utils.is_ticket_message(message):
            if payload.emoji.name == config.Ticket_info_reaction:
                await message.remove_reaction(payload.emoji, user)
                await ticket_state(channel)

            if any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in user.roles):
                if payload.emoji.name == config.Close_ticket_reaction:
                    await message.remove_reaction(payload.emoji, user)
                    config.Cursor.execute("SELECT * FROM tickets WHERE channel_id = ?;", [channel.id])
                    data = config.Cursor.fetchone()
                    if data[4] == "open":
                        await ticket_close(channel)

                    else:
                        await ticket_reopen(channel)

                elif payload.emoji.name == config.Admin_ticket_reaction:
                    await channel.set_permissions(config.Ticket_mod_role, overwrite=None)
                    update_ticket_db(channel.id, admin_mode=1)
                    await channel.send("Das Ticket wurde in den Admin-Modus gesetzt!")

                elif payload.emoji.name == config.Self_assign_ticket_reaction:
                    await message.remove_reaction(payload.emoji, user)
                    await do_assign(user, channel, user)

                elif payload.emoji.name == config.Unassign_ticket_reaction:
                    await message.remove_reaction(payload.emoji, user)
                    await do_assign(None, channel, user)

        if any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in user.roles):

            if utils.is_close_ticket_message(message):
                if payload.emoji.name == config.Accept_reaction:
                    config.Cursor.execute("SELECT * FROM additional_users WHERE ticket_id = ?", [data[0]])
                    users_in_db = config.Cursor.fetchall()
                    additional_users = []
                    for x in users_in_db:
                        additional_users.append(await config.bot.fetch_user(x[1]))
                    if data[4] == "open":
                        await channel.send("Ticket geschlossen von {0}.".format(user))
                        await message.delete()

                        overwrites = {
                            config.Guild.default_role: discord.PermissionOverwrite.from_pair(
                                discord.Permissions.none(),
                                discord.Permissions.all_channel()),
                            config.Guild.me: discord.PermissionOverwrite.from_pair(
                                discord.Permissions.all_channel(),
                                discord.Permissions.none()),
                            config.Ticket_admin_role: discord.PermissionOverwrite.from_pair(
                                discord.Permissions.all_channel(),
                                discord.Permissions.none()),
                            config.Ticket_mod_role: discord.PermissionOverwrite.from_pair(
                                discord.Permissions.text(),
                                discord.Permissions.none())
                        }

                        await channel.edit(overwrites=overwrites)
                        update_ticket_db(channel.id, status="closed")
                        await send_ticket_closed_message(channel)
                        creator = await config.bot.fetch_user(data[2])
                        if data[6]:
                            reason = data[6]
                        else:
                            reason = "Kein Grund angegeben"
                        await creator.send(
                            "Das Ticket {}; Kanalname: {} wurde geschlossen. Grund: {}".format(data[0],
                                                                                               channel.name,
                                                                                               reason))
                        for x in additional_users:
                            await x.send(
                                "Das Ticket {}; Kanalname: {} wurde geschlossen. Grund: {}".format(data[0],
                                                                                                   channel.name,
                                                                                                   reason))
                    else:
                        await channel.send("Ticket von {0} wiedereröffnet.".format(user))
                        await message.delete()

                        overwrites = {
                            config.Guild.default_role: discord.PermissionOverwrite.from_pair(
                                discord.Permissions.none(),
                                discord.Permissions.all_channel()),
                            config.Guild.me: discord.PermissionOverwrite.from_pair(
                                discord.Permissions.all_channel(),
                                discord.Permissions.none()),
                            config.Ticket_admin_role: discord.PermissionOverwrite.from_pair(
                                discord.Permissions.all_channel(),
                                discord.Permissions.none()),
                            config.Ticket_mod_role: discord.PermissionOverwrite.from_pair(
                                discord.Permissions.text(),
                                discord.Permissions.none()),
                            await config.bot.fetch_user(data[2]): discord.PermissionOverwrite.from_pair(
                                discord.Permissions.text(),
                                discord.Permissions.none())
                        }
                        await channel.edit(overwrites=overwrites)

                        for x in additional_users:
                            await channel.set_permissions(x, overwrite=discord.PermissionOverwrite.from_pair(
                                discord.Permissions.text(), discord.Permissions.none()))

                        update_ticket_db(channel.id, status='open')
                        await send_ticket_opened_message(channel)

                elif payload.emoji.name == config.Reject_reaction:
                    if data[4] == "open":
                        await channel.send("Ticket nicht geschlossen!")
                        await message.delete()
                    else:
                        await channel.send("Ticket nicht wiedereröffnet!")
                        await message.delete()
                else:  # Do nothing if User has clicked on Close_ticket_reaction. Remove Reaction
                    await message.remove_reaction(payload.emoji, user)

            elif utils.is_reopen_ticket_message(message):
                if payload.emoji.name == config.Reopen_ticket_reaction:
                    await message.remove_reaction(payload.emoji, user)
                    await ticket_reopen(channel)
                elif payload.emoji.name == config.Ticket_info_reaction:
                    await message.remove_reaction(payload.emoji, user)
                    await ticket_state(channel)
                elif payload.emoji.name == config.Delete_ticket_reaction:
                    await message.remove_reaction(payload.emoji, user)
                    await send_delete_ticket_message(channel, user)

            elif all(x.emoji in [config.Delete_ticket_reaction, config.Reject_reaction,
                                 config.Accept_reaction] for x in message.reactions):
                if payload.emoji.name == config.Accept_reaction:
                    if config.Ticket_admin_role in user.roles:
                        await delete_ticket(channel)
                    else:
                        await channel.send("Das ist nur für Ticket-Admins erlaubt!")
                elif payload.emoji.name == config.Reject_reaction:
                    await message.delete()
                    await channel.send("Löschen abgebrochen")

        else:  # No known scenario, eg. unknown reaction. Remove Reaction
            await message.remove_reaction(payload.emoji, user)


def update_ticket_db(channel_id, assigned=None, status=None, admin_mode=None, close_reason=None, reopen_reason=None):
    config.Cursor.execute("SELECT * FROM tickets WHERE channel_id = ?", [channel_id])
    data = config.Cursor.fetchone()
    if not assigned:
        assigned = data[3]
    if not status:
        status = data[4]
    if not admin_mode:
        admin_mode = data[5]
    if not close_reason:
        close_reason = data[6]
    if not reopen_reason:
        reopen_reason = data[7]
    config.Cursor.execute("REPLACE INTO tickets Values(?, ?, ?, ?, ?, ?, ?, ?)", [data[0], data[1], data[2], assigned,
                                                                                  status, admin_mode, close_reason,
                                                                                  reopen_reason])
    config.Connection.commit()


async def ticket_state(channel):
    config.Cursor.execute("SELECT * FROM tickets WHERE channel_id = ?;", [channel.id])
    data = config.Cursor.fetchone()
    reason = ""
    if data[3] >= 0:
        assigned = await config.bot.fetch_user(data[3])
    else:
        assigned = 'Aktuell kein zugewiesener Bearbeiter.'
    if data[4] == 'open':
        state = 'offen'
        if data[7]:
            reason = "\nBegründung: " + data[7]
    elif data[4] == 'closed':
        state = 'geschlossen'
        if data[6]:
            reason = "\nBegründung: " + data[6]
    else:
        state = 'Unbekannt'
    admin_mode = 'Aktiv' if data[5] else 'Inaktiv'
    config.Cursor.execute("SELECT * FROM additional_users WHERE ticket_id = ?;", [data[1]])
    additional_list = config.Cursor.fetchall()
    if not additional_list:
        user_list = ["Niemand"]
    else:
        user_list = []
        for x in additional_list:
            user = await config.bot.fetch_user(x)
            if user:
                user_list.append(user.name)
            else:
                user_list.append("Unbekannter User. Möglicherweise ein config.bot?")
    embed = discord.Embed(title="Aktueller Status von Ticket {0}".format(data[0]),
                          description="Eröffnet von: {0}\n"
                                      "Bearbeiter: {1}\n"
                                      "Status: {2}\n"
                                      "Adminmode: {3}\n"
                                      "Weitere User: {4}{5}"
                          .format(await config.bot.fetch_user(data[2]), assigned, state,
                                  admin_mode, ", ".join(user_list), reason))
    await channel.send(embed=embed)


async def ticket_close(channel):
    embed = discord.Embed(title="Möchtest du dieses Ticket wirklich schließen?",
                          description="Klicke auf {0} um das Ticket zu schließen\n"
                                      "Klicke auf {1} um das Ticket offen zu lassen"
                          .format(config.Accept_reaction, config.Reject_reaction))
    message = await channel.send(embed=embed)
    await utils.make_close_ticket_message(message)


async def ticket_reopen(channel):
    embed = discord.Embed(title="Möchtest du dieses Ticket wieder öffnen?",
                          description="Klicke auf {0} um das Ticket wieder zu öffnen\n"
                                      "Klicke auf {1} um das Ticket geschlossen zu lassen"
                          .format(config.Accept_reaction, config.Reject_reaction))
    message = await channel.send(embed=embed)
    await utils.make_close_ticket_message(message)


async def send_delete_ticket_message(channel, sender):
    if config.Ticket_admin_role in sender.roles:
        embed = discord.Embed(title="Möchtest du dieses Ticket wirklich unwideruflich Löschen?",
                              description="Klicke auf {0} um das Ticket zu löschen\n"
                                          "Klicke auf {1} um das Ticket nicht zu löschen"
                              .format(config.Accept_reaction, config.Reject_reaction))
        message = await channel.send(embed=embed)
        await utils.make_delete_ticket_message(message)
    else:
        await channel.send("Das ist nur für Ticket-Admins erlaubt!")


async def delete_ticket(channel):
    config.Cursor.execute("DELETE FROM tickets WHERE channel_id = ?", [channel.id])
    await channel.delete()
    config.Connection.commit()


async def do_assign(user, channel, assigner):
    if any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in assigner.roles):
        if user:
            assigned = user.id
            await channel.send("Das Ticket wurde {0} zugewiesen".format(user))
        else:
            assigned = -1
            await channel.send("Die Ticketzuweisung wurde entfernt")
        update_ticket_db(channel.id, assigned=assigned)
    else:
        await channel.send("{0.mention} Du hast nicht die Berechtigung um dem Ticket einen Bearbeiter zuzuweisen"
                           .format(assigner))


async def send_ticket_opened_message(ticket_channel):
    await ticket_channel.purge(limit=100, check=utils.is_me)
    await ticket_state(ticket_channel)
    embed = discord.Embed(title="Ticketsystem", description="**Bitte beschreibe dein Anliegen möglichst genau!**\n\n"
                                                            "Wenn du die Ticketinfos sehen willst,"
                                                            " dann klicke auf: {}\n\n"
                                                            "__**Folgendes ist nur möglich, "
                                                            "wenn du die Rechte dazu hast:**__\n"
                                                            "Wenn das Ticket nur von einem Admin bearbeitet "
                                                            "werden soll, klicke auf: {}\n"
                                                            "Wenn du das Ticket schließen willst klicke auf: {}\n"
                                                            "Wenn du das Ticket dir zuweisen möchtest klicke auf: {}\n"
                                                            "Wenn du die Zuweisung aufheben möchtest klicke auf: {}"
                          .format(config.Ticket_info_reaction, config.Admin_ticket_reaction,
                                  config.Close_ticket_reaction, config.Self_assign_ticket_reaction,
                                  config.Unassign_ticket_reaction))
    message = await ticket_channel.send(embed=embed)
    await utils.make_ticket_message(message)


async def send_ticket_closed_message(channel):
    await channel.purge(limit=100, check=utils.is_me)
    await ticket_state(channel)
    embed = discord.Embed(title="Dieses Ticket ist aktuell geschlossen!",
                          description="Klicke auf {0} um das Ticket wieder zu öffnen\n"
                                      "Klicke auf {1} um die Tickteinfos anzeigen zu lassen\n"
                                      "Klicke auf {2} um das Ticket endgültig zu löschen"
                          .format(config.Reopen_ticket_reaction, config.Ticket_info_reaction, config.Delete_ticket_reaction))
    message = await channel.send(embed=embed)
    await utils.make_reopen_ticket_message(message)
