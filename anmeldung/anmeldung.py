import discord

import config
from utils import views


__anmeldung_deny_permission = discord.Permissions(1 << 36) # eigentlich create_private_threads
__anmeldung_permission = discord.Permissions.all_channel() ^ __anmeldung_deny_permission

async def create_channel(user, title, payload):
    overwrites = {
        # default-Berechtigungen: keine
        config.Guild.default_role: discord.PermissionOverwrite.from_pair(discord.Permissions.none(),
                                                                         discord.Permissions.all_channel()),
        # Bot darf alles
        config.Guild.me: discord.PermissionOverwrite.from_pair(discord.Permissions.all_channel(),
                                                               discord.Permissions.none()),
        # Rolle Anmeldung darf alles außer privaten Thread anlegen (um solche Kanäle eindeutig zu erkennen, das Recht wird nicht gebraucht)
        config.Anmeldung_role: discord.PermissionOverwrite.from_pair(__anmeldung_permission,
                                                                     __anmeldung_deny_permission), # dunno how to correctly specify this
        # user, der sich anmelden will, darf bestimmte Sachen
        user: discord.PermissionOverwrite.from_pair(discord.Permissions(config.Private_allow_permissions),
                                                    discord.Permissions(config.Private_deny_permissions))
    }
    # Kategorie ist die gleiche, in der der Channel ist, in dem die Nachricht ist, auf die reagiert wurde
    cat = config.Anmeldung_category
    channel = await config.Guild.create_text_channel(title, category=cat, overwrites=overwrites)
    # to be able to deal with user renaming, write the user id in the topic:
    channel.topic = f"id {user.id}"
    return channel

def is_channel_user(channel, user):
    if channel.topic == None: 
        # old channel, hope that the users didn't rename
        return channel.name.endswith(user.name)
    else:
        # channel topic is user id, see above
        return channel.topic == f"id {user.id}"

def is_anmeldung_channel(channel):
    # muss in der richtigen Kategorie sein:
    if channel.category != config.Anmeldung_category:
        return False
    # in Anmeldekanälen darf Anmeldung_role alles außer privaten Thread anlegen, s.o.
    ovr = channel.overwrites_for(config.Anmeldung_role)
    if ovr is not None:
        perms = ovr.pair()
        return perms[0] == (__anmeldung_permission) and perms[1] == __anmeldung_deny_permission
    return False

def is_elternarchiv_channel(channel):
    # Elternarchiv hat dieselbe Kategorie wie der Eingangskanal, aber ist kein Anmelde-Kanal
    return channel.category == config.Anmeldung_category and channel != config.First_join_channel and not is_anmeldung_channel(channel)

def is_anmeldung_message(msg):
    # muss entweder im Einganskanal oder im Elternarchiv sein
    ch = msg.channel
    return ch == config.First_join_channel or is_elternarchiv_channel(ch)

async def close_channel(payload):
    # muss Anmeldung sein
    if is_anmeldung_channel(payload.message.channel):
        view = views.CloseChannel(payload.message.channel, "```fix\nSoll dieser Kanal wirklich geschlossen werden?```")
        await view.show()
    else:
        await payload.message.channel.send("Kein Anmeldekanal!")


async def do_anmeldung(user, payload):
    do_msg = False
    # User bekommt Neuling-Rolle (in beiden Fällen), wenn noch nicht vorhanden
    if config.Neuling_role not in user.roles:
        await user.add_roles(config.Neuling_role)
        do_msg = True
    payload_channel = await config.bot.fetch_channel(payload.channel_id)
    if do_msg:
        is_archiv = is_elternarchiv_channel(payload_channel)
        if config.Alte_freischaltung:
            # Kanal ist fix
            channel = config.Anmeldungs_channel
        else:
            # Neue Freischaltung: einen Kanal pro User
            channel = await create_channel(user, ("🎮 Wiederanmeldung-" if is_archiv else "🎮 Anmeldung-") + user.name, payload)
        if is_archiv:
            # unabhängig ob alt oder neu den Begrüßungstext
            embed = discord.Embed(title="Erneute Anmeldung",
                                description="Um dein Kind nach längerer Inaktivität erneut anzumelden ist ein weiterer Anmeldevorgang notwendig.\n"
                                            "Dieser kann aber schneller abgewickelt werden, da ihr ja das meiste schon kennt.\n\n"
                                            "Bitte schreibe hier, zu welchen Terminen du Zeit für ein Gespräch hättest.",
                                    color=0x33dd33)
            await channel.send(embed=embed)
        else:
            # unabhängig ob alt oder neu den Begrüßungstext
            embed = discord.Embed(title="Hallo und herzlich Willkommen!",
                                description="Ich bin Alfred und unterstütze das Team bei diversen Aufgaben. \n"
                                            "Bitte lies diese Nachricht aufmerksam durch und befolge die für dich zutreffenden "
                                            "Punkte.\n\n"
                                            "**Damit dein Kind auf unserem Server spielen kann ist ein Gespräch zwischen unserem Team und euch "
                                            "(mindestens ein Elternteil und das Kind oder die Kinder) erforderlich.**\n\n"
                                            "Vor dem Gespräch solltest du"
                                            " die Regeln gelesen haben. Diese findest du hinter folgendem Link: "
                                            "<https://minecraft-kids.online/lexicon/entry/4-anmeldung/> \n\n"
                                            "Falls ihr auf der **Switch** spielt solltet ihr zur Vorbereitung diese Anleitung"
                                            " durcharbeiten: <https://minecraft-kids.online/lexicon/entry/16-nintendo-switch-"
                                            "mit-minecraft-server-verbinden/> \n "
                                            "Falls ihr auf der **Playstation** oder **XBox** spielt ist der einfachste Weg eine "
                                            "Android- oder iOS-App zu verwenden: Bitte schreib das kurz in deine erste "
                                            "Nachricht, damit wir dir den passenden Link zukommen lassen können.\n"
                                            "Für die **XBox** kann alternativ auch folgende Anleitung verwendet werden: "
                                            "<https://minecraft-kids.online/lexicon/entry/18-microsoft-xbox-mit-minecraft-server-verbinden/>\n"
                                            "Für **alle anderen Bedrock Geräte (Windows 10 oder 11 / iOS / Android / Smartphone / Tablet)** "
                                            "helfen wir euch gerne bei der Einrichtung, falls ihr Hilfe benötigt.\n"
                                            "Für alle **Java**-Spieler haben wir diese Anleitung vorbereitet(Als Version "
                                            "bitte aktuell die 1.20.1 verwenden.): "
                                            "<https://minecraft-kids.online/lexicon/entry/5-minecraft-launcher-pc-mac/> \n\n"
                                            "Wenn Probleme bei der Einrichtung eintreten bitte kurz hier melden, "
                                            "damit wir das beim Gespräch berücksichtigen können. \n\n"
                                            "Es ist von Nöten, einen Discord-Account für euer Kind und einen für euch zu erstellen. "
                                            "Das dient dazu, dass wir immer wissen mit wem wir gerade reden und euch "
                                            "als Eltern erreichen können, falls wir Gesprächsbedarf haben. Warum das so ist "
                                            " erfahrt ihr unter folgendem Link: <https://minecraft-kids.online/lexicon/entry/4-anmeldung/>\n"
                                            "Wir haben eine Anleitung geschrieben, dass euer Kind nicht "
                                            "außerhalb dieses Discords von Mitgliedern dieses oder anderer Discords angeschrieben"
                                            " werden kann: <https://minecraft-kids.online/lexicon/entry/3-discord-privatsph%C3%A4re-sicherheit/> \n"
                                            "Die Anpassungen werden von uns dringend empfohlen. Falls dein Kind außerhalb "
                                            "dieses Discords kommuniziert, liegt das außerhalb unseres Einflussbereiches. "
                                            "Was dort passiert können wir weder hören noch sehen.\n\n"
                                            "Wir freuen uns auf Deine Nachricht.",
                                    color=0x33dd33)
            await channel.send(embed=embed)
            await channel.send("{}, deine erste Nachricht schreibst du am besten auf Basis "
                            "dieser Vorlage:\n"
                            "Welche Minecraft-Plattform wird genutzt? (Java, Switch, Playstation, xBox, iOS, Android oder Windows10)\n"
                            "Regeln gelesen und einverstanden? (Ja/nein)\n"
                            "Getrennte Discord-Accounts für Eltern und Kinder vorhanden? (ja/nein)\n"
                            "Empfohlene Sicherheitseinstellungen umgesetzt? (ja/nein)\n"
                            "Mögliche Termine für das Gespräch: (mindestens 3 Termine. Bitte mit Datum und Uhrzeit, z.B. 'Kann am 10.12.2033 von 12Uhr bis 15Uhr')\n"
                            "Wenn du Hilfe brauchst schreib einfach in diesen Chat und es wird sich jemand bei dir melden um dich zu unterstützen.\n".format(user.mention))
    else:
        # Benutzer hat schon die Rolle, sollte die Reaction die hie4rzu geführt hat eigentlich gar ni cht mehr sehen!
        # -> Meldung in Spam-Kanal
        await config.Spam_channel.send(f"{user.name} kann {payload_channel} noch sehen, obwohl er bereits die Neuling-Rolle hat!")
        # und Meldung an den User, damit Feedback kommt
        view = views.Notify(payload_channel, "```fix\nDu hast bereits einen Anmeldekanal. Gehe bitte dorthin.```")
        await view.show()

async def do_bewerbung(user, payload):
    do_msg = False
    # Bewerber bekommt die Rolle, damit Eingang nicht mehr sichtbar
    if config.Bewerber_role not in user.roles:
        await user.add_roles(config.Bewerber_role)
        do_msg = True
        channel = config.Bewerber_channel
    # weitermachen nur, wenn noch nicht Bewerber
    if do_msg:
        if config.Alte_freischaltung:
            # alt: fester Kanal für Bewerber
            channel = config.Bewerber_channel
        else:
            # neu: jeder seinen eigenen Kanal
            channel = await create_channel(user, '💼 Bewerbung-' + user.name, payload)
        # auf jeden Fall die Begrüßungstexte
        embed = discord.Embed(title="Hallo und herzlich Willkommen!",
                              description="Ich bin Alfred und unterstütze das Team bei diversen Aufgaben.\n"
                              "Bitte lies dir die folgende Nachricht aufmerksam durch!",
                              color=0x33dd33)
        await channel.send(embed=embed)
        msg = "Hallo {}, schön, dass Du da bist!\n\nEs freut uns, dass Du Dich mit dem Gedanken angefreundet hast, unser Elternprojekt "\
              " für Kinder mit Deinem Know-how zu unterstützen. Hier hast Du die Möglichkeit, mit uns "\
              "in Ruhe einen Termin für ein gemeinsames Gespräch im Audio-Kanal zu finden. "\
              "Bitte gib uns doch einfach **drei mögliche Termine** vor, bestehend aus **Uhrzeit** und **Datum**. "\
              "Bitte hab Verständnis, dass wir __ausschließlich Gespräche oder Videokonferenzen führen__ "\
              "und keine Bewerbungen via Textchat akzeptieren.\n\n"\
              "Wir alle aus dem Eltern-Team machen unsere Aufgaben ehrenamtlich, neben Familie und Beruf. "\
              "Daher können wir auch nicht immer alle Termine \"zu sofort\" wahrnehmen und benötigen ggf. etwas Vorlauf, "\
              "um uns auf das Gespräch mit Dir vorzubereiten. Damit das Gespräch nicht für beide Seiten den Rahmen sprengt, "\
              "wäre für uns wichtig zu wissen, welches Know-how Du mitbringst und wo Du Dich gerne bei uns einbringen willst.\n\n"\
              "__**Wo braucht ihr denn genau Hilfe?**__\n\n"\
              "- **Support** (Discord-Überwachung und auf dem Server helfen und gelegentliche Events veranstalten)\n"\
              "- **Marketing** (für Social Media, Werbung für unsere Plattform)\n"\
              "- **Redakteure** (Website & Tutorials verfassen und aktualisieren)\n"\
              "- **Technik**  (Plugin- und (Root-)Server-Aktualisierungen und Wartungsarbeiten, Plugin-Programmierung (Java))\n\n"\
              "Aufgaben und Inhalte einzelner Teammitglieder können im Gespräch individuell vereinbart werden und "\
              "auf Grund von fachlicher Kompetenz auch voneinander abweichen.".format(user.mention)
        await channel.send(msg)
        msg = "\n\nAus Jugend- und Datenschutzgründen und zur Eigensicherung müssen alle Mitglieder im Eltern-Team "\
              "die folgenden verlinkten Nutzungsbedingungen akzeptieren, dann **(die letzte) Seite 5 ausgefüllt** "\
              "an unsere E-Mail-Adresse übermitteln. Um die Echtheit der Daten zu bestätigen, bieten wir 2 Varianten:\n"\
              "- Videokonferenz im Discord, Abgleich der personenbezogenen Daten mittels Personalausweis in die Kamera.\n"\
              "- Übermittlung der Unterlagen an unsere E-Mail- oder Fax-Adresse; **leserlich und beide Seiten des Personalausweises bitte**.\n\n"\
              "**Ohne eine Bestätigung der Echtheit Deiner Daten werden wir niemanden aufnehmen oder "\
              "für unsere geschlossenen Bereiche und Systeme freischalten, dies dient zum Schutz aller Kinder hier auf unserfer Plattform!**\n\n"\
              "Sollte es noch Fragen zu den Nutzungsbedingungen oder sonstigen Angelegenheiten geben, kann dies gerne im Gespräch geklärt werden.\n\n"\
              "**Nutzungsbedingungen Download**\nhttps://pad.minecraft-kids.online/file/#/2/file/DxKpzeNyd62pzfAu9Vy0NkkF/\n\n"\
              "**Kontakt-Daten**\n"\
              "E-Mail: support@minecraft-kids.online\n"\
              "Telefon: +49 (0)69 24 75 39 468\n"\
              "Fax: +49 (0)511 37 36 10 14"
        await channel.send(msg)
