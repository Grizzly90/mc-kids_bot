from aiohttp import web
import logging
import config
import discord


async def handle(request):
    name = request.match_info.get('name', "Anonymous")
    text = "Hello, " + name
    return web.Response(text=text)


async def send_web_message(request):
    data = await request.post()
    message = None
    message = data['message']
    secret = data['secret']
    if (secret != config.Secret) | (not message):
        status = 404
        print("falsches secret oder leere nachricht")
        return web.Response(status=status)
    target_channel = request.match_info.get('target_channel')
    try:
        channel_id = int(target_channel)
        status = 200
    except:
        for channel_list_entry in config.Channel_list:
            if channel_list_entry[0].lower() == target_channel.lower():
                channel_id = channel_list_entry[1]
        try:
            channel_id
            status = 200
        except:
            channel_id = 0
            status = 404
            print("channelnamen nicht in der config gefunden")
    try:
        channel = await config.bot.fetch_channel(channel_id)
        await channel.send(message)
    except:
        status = 404
        print("channel nicht im discord vorhanden")
    return web.Response(status=status)


async def send_announcement(request):
    data = await request.post()
    secret = data['secret']
    embed_type = int(data['embed_type'])
    colour = discord.Colour(int(data['colour'].replace("#", "0x"), 16))
    if embed_type > 0:
        img_url = data['img_url']
    if (secret != config.Secret) | (not data['description']) | (not data['title']):
        status = 404
        print("falsches secret oder leere nachricht")
        return web.Response(status=status)
    else:

        description = data['description']
        title = data['title']
        target_channel = request.match_info.get('target_channel')
        try:
            channel_id = int(target_channel)
        except:
            for channel_list_entry in config.Channel_list:
                if channel_list_entry[0].lower() == target_channel.lower():
                    channel_id = channel_list_entry[1]
            try:
                channel_id
            except:
                channel_id = 0
                print("channelnamen nicht in der config gefunden")
        try:
            channel = await config.bot.fetch_channel(channel_id)
        except:
            print("channel nicht im discord vorhanden")

        if data['everyone'] == 1:
            await channel.send("@everyone")
        embed = discord.Embed(title=title, description=description, color=colour)
        if embed_type == 1:
            embed.set_image(url=img_url)
        elif embed_type == 2:
            embed.set_thumbnail(url=img_url)
        await channel.send(embed=embed)

        return web.HTTPFound('/bot/announement/{}'.format(target_channel))
        # await provide_form(request)


async def provide_form(request):
    channel = request.match_info.get('target_channel')
    text = '<html><body><form action="{}/bot/send/announement/{}" method="post" ' \
           'accept-charset="utf-8" enctype="application/x-www-form-urlencoded">' \
           '<table><tr><th>&nbsp;</th><th>&nbsp;</th></tr>' \
           '<tr><td><label for="title">Title</label></td>' \
           '<td><input id="title" name="title" type="text" value="" autofocus/></td></tr>' \
           '<tr><td><label for="description">Description</label></td>' \
           '<td><textarea rows="8" cols="80" id="description" name="description" type="text" value="">' \
           '</textarea></td></tr>' \
           '<tr><td><label for="secret">Secret</label></td>' \
           '<td><input id="secret" name="secret" type="password" value=""/></td></tr>' \
           '<tr><td>&nbsp;</td><td><label for="secret"><input type="hidden" value=0 name="everyone">' \
           '<input type="checkbox" name="everyone" value=1> everyone</label></td></tr>' \
           '<tr><td><label for="colour">Colour</label></td>' \
           '<td><input type="color" id="colour" name="colour"' \
           'value="#05cc51"></td></tr>' \
           '<tr><td><label for="emebd_type">Embed Type</label></td>' \
           '<td><select name="emebd_type">' \
           '<option value=0 selected>embed</option>' \
           '<option value=1>image embed</option>' \
           '<option value=2>thubnail embed</option>' \
           '</select></td></tr>' \
           '<tr><td><label for="img_url">URL for image oder thubnail</label></td>' \
           '<td><input id="img_url" name="img_url" type="text" value=""/></td></tr>' \
           '<tr><td>&nbsp;</td><td><input type="submit" value="Senden"/></td></tr></table></body></html>'\
        .format(config.Form_url, channel)
    return web.Response(text=text, content_type="text/html")


app = web.Application()
app.add_routes([web.get('/bot/', handle),
                web.get('/bot/{name}', handle),
                web.post('/bot/msg/{target_channel}', send_web_message),
                web.post('/bot/send/announement/{target_channel}', send_announcement),
                web.get('/bot/announement/{target_channel}', provide_form)])


async def webserver():
    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, '0.0.0.0', 5000)
    await site.start()
