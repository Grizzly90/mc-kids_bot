import re

import discord

# import commands
import config
from utils import utils
from ticketsystem import ticketsystem
from anmeldung import anmeldung
from platformroles import platformroles
# requires mcstatus and is buggy anyway from status import status


# ToDo: Move ticket commands to a new Module
# ToDo: Add comments


@config.bot.command(
    aliases=["com"],
    brief="Sendet die Nachricht um neue Tickets zu eröffnen",
    description="Sendet die Nachricht um neue Tickets zu eröffnen"
)
async def createopenticketmessage(ctx):
    if not ctx.channel == config.Open_ticket_channel:
        await ctx.channel.send("Das ist nur im Channel ``{0}`` möglich!".format(config.Open_ticket_channel))
        return
    if config.Ticket_admin_role not in ctx.author.roles:
        await ctx.channel.send("Das ist nur für Ticket-Admins erlaubt!")
        return
    await config.Open_ticket_channel.purge()
    text = "Klicke auf die Reaktion {} unter dieser Nachricht um ein neues Ticket zu erstellen.\n" \
           "Daraufhin wird ein neuer Textkanal erstellt wo du dein anliegen beschreiben kannst." \
        .format(config.Open_ticket_reaction)
    embed = discord.Embed(title="Ticketsystem", description=text)
    config.Open_ticket_message = await config.Open_ticket_channel.send(embed=embed)
    await config.Open_ticket_message.add_reaction(config.Open_ticket_reaction)
    config.Ticket_category = discord.utils.get(config.Open_ticket_message.guild.categories,
                                               name=config.Tickets_category_name)
    config.Cursor.execute("REPLACE INTO variables VALUES('Open_ticket_message', ? )",
                          [str(config.Open_ticket_message.id)])
    config.Connection.commit()


@config.bot.command(
    aliases=["add", "benutzer_hinzufügen"],
    brief="Fügt einen Benutzer zum Ticket hinzu",
    description="Fügt einen Benutzer zum Ticket hinzu",
    usage="Benutzer"
)
async def adduser(ctx, user: discord.Member):
    await ctx.message.delete()
    channel = ctx.message.channel
    # TODO: simplify the part for tickets, no need to store the added users in the database
    if anmeldung.is_anmeldung_channel(channel) and config.Anmeldung_role in ctx.author.roles:
        user_permissions = channel.overwrites_for(user).pair()
        if user_permissions[0].value > 0:
            await ctx.channel.send("{0} ist bereits hinzugefügt.".format(user))
        else:
            # see anmeldung.create_channel
            await ctx.channel.set_permissions(user, overwrite=discord.PermissionOverwrite.from_pair(
                discord.Permissions(config.Private_allow_permissions), discord.Permissions(config.Private_deny_permissions)))  
            await ctx.channel.send("{0.mention} wurde hinzugefügt.".format(user))
    else:
        if not any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in ctx.author.roles):
            await channel.send("Dazu bist du nicht berechtigt!")
            return
        config.Cursor.execute("SELECT * FROM tickets WHERE channel_id = ?", [channel.id])
        data = config.Cursor.fetchone()
        user_permissions = channel.overwrites_for(user).pair()
        if channel.category == config.Ticket_category:
            if user_permissions[0].value > 0:
                await ctx.channel.send("{0} ist bereits Mitglied dieses Textkanals.".format(user))
            elif user.id == data[2]:
                await ctx.channel.set_permissions(user, overwrite=discord.PermissionOverwrite.from_pair(
                    discord.Permissions(523328), discord.Permissions(12288)))
                await ctx.channel.send("Der Ersteller des Tickets Nr. {0}: {1.mention} wurde hinzugefügt."
                                    .format(data[0], user))
            else:
                await ctx.channel.set_permissions(user, overwrite=discord.PermissionOverwrite.from_pair(
                    discord.Permissions(523328), discord.Permissions(12288)))
                await ctx.channel.send("{0.mention} wurde hinzugefügt.".format(user))
                config.Cursor.execute("INSERT INTO additional_users VALUES(?, ?)", [str(data[0]),
                                                                                    str(user.id)])
                config.Connection.commit()


@config.bot.command(
    aliases=["remove", "benutzer_löschen"],
    brief="Entfernt einen Benutzer vom Ticket",
    description="Entfernt einen Benutzer vom Ticket",
    usage="Benutzer"
)
async def rem(ctx, user: discord.Member):
    await ctx.message.delete()
    channel = ctx.message.channel
    user_permissions = channel.overwrites_for(user).pair()
    # TODO: simplify the part for tickets, no need to store the added users in the database
    if anmeldung.is_anmeldung_channel(channel) and config.Anmeldung_role in ctx.author.roles:
        # don't allow to remove the one this thread belongs to:
        if anmeldung.is_channel_user(channel, user):
            await ctx.channel.send(user.name + " darf nicht entfernt werden.")
        elif user_permissions[0].value > 0: # was added
            await ctx.channel.set_permissions(user, overwrite=None)
            await ctx.channel.send(user.name + " wurde entfernt.")
        else:
            await channel.send("{0} ist kein Mitglied dieses Kanals.".format(user))
    else:
        if not any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in ctx.author.roles):
            await channel.send("Dazu bist du nicht berechtigt!")
            return
        if channel.category == config.Ticket_category:
            config.Cursor.execute("SELECT * FROM tickets WHERE channel_id = ?", [channel.id])
            data = config.Cursor.fetchone()
            config.Cursor.execute("SELECT * FROM additional_users WHERE ticket_id = ?", [data[0]])
            additional_users = config.Cursor.fetchall()
            if any(x[1] == user.id for x in additional_users):
                await ctx.channel.set_permissions(user, overwrite=None)
                await ctx.channel.send(user.name + " wurde entfernt.")
                config.Cursor.execute("DELETE FROM additional_users WHERE user = ?", [user.id])
                config.Connection.commit()
            elif user_permissions[0].value > 0:  # User is the ticket creator
                await ctx.channel.set_permissions(user, overwrite=None)
                await ctx.channel.send("Der Ersteller des Tickets Nr. {0}: {1} wurde entfernt.".format(data[0], user.name))
            else:
                await channel.send("{0} ist kein Mitglied dieses Kanals.".format(user))


@config.bot.command(
    aliases=["begründung"],
    brief="Hinterlegt einen Grund zum öffnen oder schließen vom Ticket",
    description="Hinterlegt einen Grund zum öffnen oder schließen vom Ticket",
    usage="Begründung"
)
async def reason(ctx, *, args):
    await ctx.message.delete()
    channel = ctx.message.channel
    if not any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in ctx.author.roles):
        await channel.send("Dazu bist du nicht berechtigt!")
        return
    if not args:
        await channel.send("Es wurde kein Grund angegeben")
        return
    config.Cursor.execute("SELECT * FROM tickets WHERE channel_id = ?", [channel.id])
    data = config.Cursor.fetchone()
    if data[4] == 'open':
        ticketsystem.update_ticket_db(channel.id, close_reason=args)
        pass
    elif data[4] == 'closed':
        ticketsystem.update_ticket_db(channel.id, reopen_reason=args)
        pass


@config.bot.command(
    aliases=["zuweisen"],
    brief="Weist das aktuelle Ticket einem benutzer zu",
    description="Weist das aktuelle Ticket einem benutzer zu",
    usage="benutzer"
)
async def assign(ctx, user: discord.User = None):
    await ctx.message.delete()
    if not user:  # No User to assign specified; use sender
        user = ctx.message.author
    await ticketsystem.do_assign(user, ctx.message.channel, ctx.author)


@config.bot.command(
    hidden=True
)
async def test(ctx, title, img, *, description):
    await ctx.message.delete()
    channel = ctx.channel
    embed = discord.Embed(title=title, description=description, color=0x05cc51)
    print(img)
    embed.set_image(url=img)
    await channel.send(embed=embed)


@config.bot.command(
    hidden=True
)
async def test2(ctx, title, img, *, description):
    await ctx.message.delete()
    channel = ctx.channel
    embed = discord.Embed(title=title, description=description, color=0x05cc51)
    embed.set_thumbnail(url=img)
    await channel.send(embed=embed)


@config.bot.command(
    help="Hebt die Ticktezuweisung auf",
    brief="Hebt die Ticktezuweisung auf")
async def unassign(ctx):
    await ticketsystem.do_assign(None, ctx.message.channel, ctx.author)


@config.bot.command(
    help="Sendet die zum jeweiligen Zeitpunkt gültige Infonachricht",
    brief="Infonachricht erneut senden"
)
async def resend(ctx):
    await ctx.message.delete()
    channel = ctx.message.channel
    if not any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in ctx.author.roles):
        await channel.send("Dazu bist du nicht berechtigt!")
        return
    config.Cursor.execute("SELECT * FROM tickets WHERE channel_id = ?;", [channel.id])
    data = config.Cursor.fetchone()
    if data[4] == 'open':
        await ticketsystem.send_ticket_opened_message(channel)
    elif data[4] == 'closed':
        await ticketsystem.send_ticket_closed_message(channel)


@config.bot.command(
    aliases=["schließen"],
    brief="Schließt das aktuelle Ticket",
    description="Schließt das aktuelle Ticket"
)
async def close(ctx):
    if not any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in ctx.author.roles):
        await ctx.channel.send("Dazu bist du nicht berechtigt!")
        return
    await ctx.message.delete()
    await ticketsystem.ticket_close(ctx.message.channel)


@config.bot.command(
    aliases=["wiederöffnen"],
    brief="Wiedereröffnet das aktuelle Ticket",
    description="Wiedereröffnet das aktuelle Ticket"
)
async def reopen(ctx):
    if not any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in ctx.author.roles):
        await ctx.channel.send("Dazu bist du nicht berechtigt!")
        return
    await ctx.message.delete()
    await ticketsystem.ticket_reopen(ctx.message.channel)


@config.bot.command(
    aliases=["umbenennen"],
    brief="Benennt das aktuelle Ticket um",
    description="Löscht das aktuelle Ticket",
    usage="neuer_name"
)
async def rename(ctx, *args):
    await ctx.message.delete()
    channel = ctx.channel
    config.Cursor.execute("SELECT * FROM tickets WHERE channel_id = ?;", [channel.id])
    data = config.Cursor.fetchone()
    user = await config.bot.fetch_user(data[2])
    if not (any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in ctx.author.roles) or ctx.author == user):
        await channel.send("Du bist nicht berechtigt den Kanalnamen zu ändern!")
        return
    await channel.edit(name="{0}_{1}".format(" ".join(args), user.name))


@config.bot.command(
    aliases=["ticket_löschen"],
    brief="Löscht das aktuelle Ticket",
    description="Löscht das aktuelle Ticket"
)
async def delete(ctx):
    await ctx.message.delete()
    if not any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in ctx.author.roles):
        await ctx.channel.send("Dazu bist du nicht berechtigt!")
        return
    await ticketsystem.send_delete_ticket_message(ctx.message.channel, ctx.author)


@config.bot.command(
    aliases=["status", "info"],
    brief="Zeigt die Ticketinformationen an",
    description="Zeigt die Ticketinformationen an"
)
async def state(ctx):
    await ctx.message.delete()
    await ticketsystem.ticket_state(ctx.message.channel)


@config.bot.command(
    brief="Fügt Reaktionen zu einer bestimmten Nachricht hinzu",
    description="Fügt Reaktionen zu einer bestimmten Nachricht hinzu",
    usage="Nachricht_ID Reaktion(1) Reaktion(n)"
)
async def reagiere(ctx, arg, *args):
    await ctx.message.delete()
    channel = ctx.channel
    if not any(x.id in [config.Ticket_admin_role_id] for x in ctx.author.roles):
        await channel.send("Dazu bist du nicht berechtigt!")
        return
    message = await channel.fetch_message(arg)
    for x in args:
        try:
            await message.add_reaction(x)
        except:
            await ctx.author.send("**{}** ist keine gültige Reaction".format(x))


@config.bot.command(
    brief="Erstellt eine Terminumfrage für ein Meeting",
    description="Erstellt eine Terminumfrage für ein Meeting",
    usage="Uhrzeit Datum(1) [Abweichende Uhrzeit] [Grund für Abweichung] Datum(n)"
)
async def meeting(ctx, *args):
    await ctx.message.delete()
    channel = ctx.channel
    if not any(x.id in [config.Ticket_admin_role_id] for x in ctx.author.roles):
        await channel.send("Dazu bist du nicht berechtigt!")
        return
    dates = []
    time = None
    temp_time = None
    day = None
    args = (*args, "the_end")
    for x in args:
        if re.search("^(([01]?[0-9]|2[0-3]):[0-5][0-9]|24:00)$", x):  # Uhrzeit
            if not time:
                time = x
            else:
                temp_time = x

        elif re.search("^([0-2]?[0-9]|[3][01])\\.([0]?[0-9]|[1][0-2])(\\.[0-9]{0,4})?$", x):  # Datum
            if temp_time:
                dates.append(day + " um " + temp_time)
                temp_time = None

            elif day:
                dates.append(day + " um " + time)

            day = x

        elif x == "the_end":
            if temp_time:
                dates.append(day + " um " + temp_time)
            else:
                dates.append(day + " um " + time)

        else:  # Fehlhafter Parameter oder String
            dates.append(day + " um " + temp_time + "; Begründung: " + x)
            temp_time = None
            pass
    await channel.send("@everyone")
    embed = discord.Embed(title="Team Meeting",
                          description="Hallo @everyone,\n"
                                      "hier die Terminvorschläge für das nächste Teammeeting.\n"
                                      "{} für ich habe an dem Termin Zeit.\n"
                                      "{} für ich habe keine Zeit"
                          .format(config.Thumbs_up_reaction, config.Thumbs_down_reaction), color=0x05cc51)
    await channel.send(embed=embed)
    for x in dates:
        embed = discord.Embed(description=x, color=0xf0ff1f)
        message = await channel.send(embed=embed)
        await message.add_reaction(config.Thumbs_up_reaction)
        await message.add_reaction(config.Thumbs_down_reaction)


@config.bot.command(
    brief="Erstellt eine Umfrage",
    description="Erstellt eine Umfrage",
    usage="Überschrift 'Beschreibung(Mehrere Wörte müssen in Anführungszeichen gesetzt werden)' ping_everyone(0 oder 1) beliebige_anzahl_abstimmungs_elemente(getrennt durch leerzeichen))"
)
async def poll(ctx, title, description, everyone: int, *voteelement):
    await ctx.message.delete()
    channel = ctx.channel
    if not any(x.id in [config.Ticket_admin_role_id] for x in ctx.author.roles):
        await channel.send("Dazu bist du nicht berechtigt!")
        return
    embed = discord.Embed(title=title, description=description, color=0x05cc51)
    await channel.send(embed=embed)
    for element in voteelement:
        embed = discord.Embed(title=element, color=0x3366FF)
        message = await channel.send(embed=embed)
        await utils.make_poll_message(message)
    if everyone > 0:
        await channel.send("@everyone")


@config.bot.command(
    brief="Erstellt eine Umfrage mit beliebiger Anzahl an Elementen zum Abstimmen",
    description="Erstellt eine Umfrage mit beliebiger Anzahl an Elementen zum Abstimmen",
    usage="ping_everyone(0 oder 1) beliebige_anzahl_abstimmungs_elemente"
)
async def shortpoll(ctx, everyone: int, *voteelement):
    await ctx.message.delete()
    channel = ctx.channel
    if not any(x.id in [config.Ticket_admin_role_id] for x in ctx.author.roles):
        await channel.send("Dazu bist du nicht berechtigt!")
        return
    if everyone == 1:
        await channel.send("@everyone")
    for element in voteelement:
        embed = discord.Embed(title=element, color=0x3366FF)
        message = await channel.send(embed=embed)
        await utils.make_poll_message(message)


@config.bot.command(
    aliases=["ankündigung"],
    brief="Erstellt eine Ankündigung",
    description="Erstellt eine Ankündigung",
    usage="Überschrift(in anführungszeichen für mehrere Worte) everyone(0 oder 1) Beschreibung"
)
async def announcement(ctx, title, everyone: int, *, description):
    await ctx.message.delete()
    channel = ctx.channel
    author = ctx.author
    await utils.send_announcement(channel, author, title, description, everyone)


@config.bot.command(
    aliases=["imgankündigung"],
    brief="Erstellt eine Ankündigung",
    description="Erstellt eine Ankündigung",
    usage="Überschrift(in anführungszeichen für mehrere Worte) everyone(0 oder 1) img-URL Beschreibung"
)
async def imgannouncement(ctx, title, everyone: int, url, *, description):
    await ctx.message.delete()
    channel = ctx.channel
    author = ctx.author
    await utils.send_announcement(channel, author, title, description, everyone, img=url)


@config.bot.command(
    aliases=["thumbankündigung"],
    brief="Erstellt eine Ankündigung",
    description="Erstellt eine Ankündigung",
    usage="Überschrift(in anführungszeichen für mehrere Worte) everyone(0 oder 1) thumb-URL Beschreibung"
)
async def thumbannouncement(ctx, title, everyone: int, url, *, description):
    await ctx.message.delete()
    channel = ctx.channel
    author = ctx.author
    await utils.send_announcement(channel, author, title, description, everyone, thumb=url)


@config.bot.command(
    aliases=["ankündigungsfarbe", "announcementcolor"],
    brief="Setzt die Farbe für eine Ankündigung die Anschließend abgesendet wird",
    description="Setzt die Farbe für eine Ankündigung die Anschließend abgesendet wird",
    usage="Farbcode(0xhex,#hex,0x#hex, farbname oder rgb(zahl, zahl, zahl)[zahl 0-255 oder 0-100%])"
)
async def announcementcolour(ctx, colour: discord.Colour = None):
    await ctx.message.delete()
    if config.Ticket_admin_role not in ctx.author.roles:
        await ctx.channel.send("Das ist nur für Ticket-Admins erlaubt!")
        return
    if colour:
        config.announcement_colour.append([ctx.message.author.id, colour])
    else:
        for user_colour in config.announcement_colour:
            if user_colour[0] == ctx.message.author:
                config.announcement_colour.remove(user_colour)
                break


@config.bot.command(
    brief="Bot stellt sich vor",
    description="Bot stellt sich vor"
)
async def introduction(ctx):
    await ctx.message.delete()
    channel = ctx.channel
    if not any(x.id in [config.Ticket_admin_role_id] for x in ctx.author.roles):
        await channel.send("Dazu bist du nicht berechtigt!")
        return

    await channel.send("@everyone")
    embed = discord.Embed(title="Meine Vorstellung", description="Hallo zusammen,\n"
                                                                 "ich möchte mich kurz vorstellen.\n"
                                                                 "Ich bin Alfred! (Manche kennen mich vielleicht aus "
                                                                 "dem TV)\n"
                                                                 "Ab sofort helfe ich euch, bei euren Anliegen.\n"
                                                                 "Wenn ihr ein Anliegen habt, dass nur vom Team gelesen"
                                                                 " werden soll, oder Themen habt die länger dauern "
                                                                 "beim abarbeiten, wie beispielsweise Maps für "
                                                                 "Minigames, dann geht einfach zum Kanal "
                                                                 "#ticket-eröffnen und klickt auf die Reaktion.\n"
                                                                 "Dann mache ich einen neuen Kanal auf wo ihr euer "
                                                                 "Anliegen beschreiben könnt.\n\n"
                                                                 "**Eine Bitte:** Das Team ist sehr beschäftigt. "
                                                                 "Macht bitte nur ein Ticket auf, wenn ihr wirklich "
                                                                 "ein Anliegen habt.", color=0x3002FC)

    await channel.send(embed=embed)


@config.bot.command(
    aliases=["cnrm"],
    brief="Sendet die Nachricht die wo die Neulingsrolle Freigeschaltet werden kann",
    description="Sendet die Nachricht die wo die Neulingsrolle Freigeschaltet werden kann"
)
async def createneulingrolemessage(ctx):
    if not ctx.channel == config.First_join_channel:
        await ctx.channel.send("Das ist nur im Channel ``{0}`` möglich!".format(config.First_join_channel))
        return
    if config.Anmeldung_role not in ctx.author.roles:
        await ctx.channel.send("Das ist nur für Anmeldung erlaubt!")
        return
    await config.First_join_channel.purge()
    text = "**Herzlich willkommen auf unserem Discord und zum Anmeldeverfahren!**\n" \
           "Damit die Anmeldung reibungslos funktioniert benötigst Du am PC ein Headset" \
           " :headphones: und ein Mikrofon. :microphone2:\n\n" \
           "Mit dem Smartphone :mobile_phone: kannst Du auch die Ohrmuschel wie beim telefonieren :calling:" \
           " verwenden. Sprechen :speaking_head: funktioniert nur in den Bereichen mit einem :loud_sound: davor." \
           " Kanäle mit einem # sind rein zum schreiben (chatten) :speech_balloon: geeignet.\n\n" \
           "**Hier kannst du nicht schreiben.**\n\nNur Eltern können Ihre Kinder :child_tone1: auf unserem " \
           "Kinder-Server anmelden.\u00A0:underage: Ohne eine Freischaltung haben weder Du noch Dein Kind " \
           "Zugang zu unserem Kinder-Server und zu weiteren Discord-Kanälen! :no_entry_sign:\n\n" \
           "Dies dient zum Schutz der Kinder.\u00A0:child_tone1:\u00A0:page_with_curl:\n\n" \
           "Bitte besuche unsere Website und mache Dich mit dem Vorgang der Freischaltung vertraut.\n" \
           "<https://minecraft-kids.online/lexicon/entry/4-anmeldung/>\n\n" \
           "**Bitte klicke auf den :point_right: {} unter diesem Text :point_down: um automatisch in unseren " \
           "Anmeldebereich zu gelangen.**\n" \
           "Ansonsten bekommst Du keinen Zugang zu uns.\n\n" \
           "**Möchtest du dich für das Supportteam bewerben? Dann klick bitte statt dessen " \
           "auf {} unter dem Text.**" \
           .format(config.Accept_reaction, config.Apply_reaction)
    config.First_join_channel_reaction_message = await config.First_join_channel.send(text)
    await config.First_join_channel_reaction_message.add_reaction(config.Accept_reaction)
    await config.First_join_channel_reaction_message.add_reaction(config.Apply_reaction)
    config.Cursor.execute("REPLACE INTO variables VALUES('First_join_channel_reaction_message', ? )",
                          [str(config.First_join_channel_reaction_message.id)])
    config.Connection.commit()
    await config.Spam_channel.send("Eingangsnachricht wurde erneuert.")

@config.bot.command(
    aliases=["eamsg"],
    brief="Nachricht für Elternarchiv erstellen",
    description="Nachricht für Elternarchiv erstellen"
)
async def elternarchivmsg(ctx):
    if not anmeldung.is_elternarchiv_channel(ctx.channel):
        await ctx.channel.send("Das ist nur im Elternarchiv möglich!")
        return
    if config.Anmeldung_role not in ctx.author.roles:
        await ctx.channel.send("Das ist nur für Anmeldung erlaubt!")
        return
    await ctx.channel.purge()
    text = "**Du befindest dich im Elternarchiv**\n" \
           "Aufgrund länger andauernder Inaktivität haben wir den(e) Kind(er) auf dem Server gesperrt" \
           " und dich in das Elternarchiv verschoben.\n\n" \
           "Wenn dein Kind wieder bei uns spielen will ist eine erneute, verkürzte Anmeldung erforderlich.\n\n" \
           "**Bitte klicke auf den :point_right: {} unter diesem Text :point_down: um diese Anmeldung einzuleiten.**" \
           .format(config.Accept_reaction)
    msg = await ctx.channel.send(text)
    await msg.add_reaction(config.Accept_reaction)
    await config.Spam_channel.send("Nachricht für Elternarchiv wurde erneuert.")

@config.bot.command(
    aliases=["crm"],
    brief="Sendet die Nachricht zum Anzeigen der Regeln",
    description="Sendet die Nachricht zum Anzeigen der Regeln"
)
async def createrulesmessage(ctx):
    if not ctx.channel == config.Rules_channel:
        await ctx.channel.send("Das ist nur im Channel ``{0}`` möglich!".format(config.Rules_channel))
        return
    if config.Ticket_admin_role not in ctx.author.roles:
        await ctx.channel.send("Das ist nur für Ticket-Admins erlaubt!")
        return
    await utils.send_rules_message()
    await config.Spam_channel.send("Regelnachricht wurde erneuert.")


@config.bot.command(
    aliases=["delchannel"],
    brief="Löscht alle Nachrichten im aktuellen Kanal",
    description="Löscht alle Nachrichten im aktuellen Kanal",
    usage="Anzahl(negativ oder weggelassen=alle)",
    pass_context=True
)
async def clearchannel(ctx, cnt: int = -1):
    if config.Ticket_admin_role not in ctx.author.roles:
        await ctx.channel.send("Das ist nur für Ticket-Admins erlaubt!")
        return
    deleted = 0
    if cnt < 0:
        deleted = await ctx.channel.purge()
    else:
        deleted = await ctx.channel.purge(limit=cnt)
    await config.Spam_channel.send('{} Nachricht(en) in {} gelöscht.'.format(len(deleted), ctx.channel.mention))

@config.bot.command(
    brief="Listet alle eigenen Emojis",
    description="Listet alle eigenen Emojis",
    pass_context=True
)
async def customemojis(ctx, cnt: int = -1):
    if config.Ticket_admin_role not in ctx.author.roles:
        await ctx.channel.send("Das ist nur für Ticket-Admins erlaubt!")
        return
    es = await config.Guild.fetch_emojis()
    print(es)
    for e in es:
        await ctx.channel.send("- {}: {}".format(e.id, e.name))


@config.bot.command(
    brief="Schließt den Anmeldekanal",
    description="Schließt den Anmeldekanal, nachdem die Anmeldung abgeschlossen ist.",
    pass_context=True
)
async def angemeldet(ctx):
    await ctx.message.delete()
    if config.Anmeldung_role not in ctx.author.roles:
        await ctx.channel.send("Das ist nur für Teammitglieder erlaubt!")
    else:
        await anmeldung.close_channel(ctx)


"""
@config.bot.command(
    aliases=["online"],
    brief="Serverstatus abfragen",
    description="Zeigt, wer aktuell auf dem Server ist.",
    pass_context=True
)
async def serverstatus(ctx):
    await ctx.message.delete()
    await status.show_status(ctx)
"""

@config.bot.command(
    brief="Plattform-Rollen-Message erzeugen",
    description="Erzeugt eine ",
    pass_context=True
)
async def platformrolesmsg(ctx):
    await platformroles.create_message(ctx)
