import discord
import requests

import config

# ToDo: Move ticket methods to new module
# ToDo: Add comments


def is_me(m):
    return m.author == config.bot.user


def is_bot_message(message, expected_reactions):
    return all(reaction.emoji in expected_reactions for reaction in message.reactions)


def is_ticket_message(message):
    return is_bot_message(message, config.Ticket_message_reactions)


def is_close_ticket_message(message):
    return is_bot_message(message, config.Close_ticket_message_reactions)


def is_reopen_ticket_message(message):
    return is_bot_message(message, config.Reopen_ticket_message_reactions)


def is_delete_ticket_message(message):
    return is_bot_message(message, config.Delete_ticket_message_reactions)


def is_poll_message(message):
    return is_bot_message(message, config.Poll_message_reactions)


async def make_bot_message(message, reactions):
    for reaction in reactions:
        await message.add_reaction(reaction)


async def make_ticket_message(message):
    await make_bot_message(message, config.Ticket_message_reactions)


async def make_close_ticket_message(message):
    await make_bot_message(message, config.Close_ticket_message_reactions)


async def make_reopen_ticket_message(message):
    await make_bot_message(message, config.Reopen_ticket_message_reactions)


async def make_delete_ticket_message(message):
    await make_bot_message(message, config.Delete_ticket_message_reactions)


async def make_poll_message(message):
    await make_bot_message(message, config.Poll_message_reactions)


async def owner_relay(message):
    embed = discord.Embed(title="Privat Nachricht von {}".format(message.author), description=message.content)
    await config.Owner.send(embed=embed)


async def send_announcement(channel, author, title, description, everyone, img=None, thumb=None, colour=0x05cc51):
    if not any(x.id in [config.Ticket_admin_role_id] for x in author.roles):
        return
    if everyone >= 1:
        await channel.send("@everyone")

    for user_colour in config.announcement_colour:
        if user_colour[0] == author.id:
            colour = user_colour[1]
            config.announcement_colour.remove(user_colour)
            break
    embed = discord.Embed(title=title, description=description, color=colour)
    if img:
        embed.set_image(url=img)
    elif thumb:
        embed.set_thumbnail(url=thumb)
    await channel.send(embed=embed)


async def send_rules_message():
    await config.Rules_channel.purge()
    text = "**Serveradresse: __`spiel.minecraft-kids.online`__**\n\n" \
           "{} Discord Regelwerk\n" \
           "{} Nutzungsbedingungen\n" \
           "{} Regelwerk Plot / Citybuild\n" \
           "{} Regelwerk Survival\n" \
           "{} Regelwerk Kreativ\n" \
           "{} Regelwerk Skyblock\n" \
           "{} Verbannungen / Stumm / Verwarnungen" \
           .format(config.Microphone_emoji, config.Pen_ballpoint_emoji, 
                config.Cityscape_emoji, config.Crossed_swords_emoji, config.Art_emoji, config.Island_emoji, 
                config.No_entry_sign_emoji)
    embed = discord.Embed(title="Minecraft-Kids Regeln", description=text, color=0x77a953)
    rules_channel_message = await config.Rules_channel.send(embed=embed)
    await make_bot_message(rules_channel_message, config.Rules_message_reactions)
    await make_bot_message(rules_channel_message, config.Rules_message_reactions)


async def call_pterodactyl_api(api, server):
    url = api["url"] + server["id"] + "/resources"
    headers = {
        "Authorization": "Bearer " + api["key"],
        "Accept": "application/json"
    }
    response = requests.request('GET', url, headers=headers)
    if response.status_code == 200:
        data = response.json()
        return data
    else:
        print(f'[Pterodactyl API] error calling {url} (status code {response.status_code}):')
        print(response.text)
