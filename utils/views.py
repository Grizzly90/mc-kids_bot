import discord

class CloseChannel(discord.ui.View):
    def __init__(self, channel, msg):
        super().__init__(timeout=60)
        self.channel = channel
        self.msgText = msg

    async def show(self):
        self.message = await self.channel.send(self.msgText,view=self)

    @discord.ui.button(label="OK", style=discord.ButtonStyle.success)
    async def okBtn(self, interaction: discord.Interaction, btn:discord.ui.Button):
        await self.channel.delete()
        self.stop()

    @discord.ui.button(label="Abbrechen", style=discord.ButtonStyle.red)
    async def cancelBtn(self, interaction: discord.Interaction, btn:discord.ui.Button):
        await self.message.delete()
        self.stop()

    async def on_timeout(self) -> None:
        await self.message.delete()
        self.stop()


class Notify(discord.ui.View):
    def __init__(self, channel, msg):
        super().__init__(timeout=60)
        self.channel = channel
        self.msgText = msg

    async def show(self):
        self.message = await self.channel.send(self.msgText,view=self)

    @discord.ui.button(label="OK", style=discord.ButtonStyle.success)
    async def okBtn(self, interaction: discord.Interaction, btn:discord.ui.Button):
        await self.message.delete()
        self.stop()

    async def on_timeout(self) -> None:
        await self.message.delete()
        self.stop()