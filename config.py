import os
import sqlite3
import json

import discord
from discord import Intents
from discord.ext import commands
from dotenv import load_dotenv

import alfred

load_dotenv()

dbExists = os.path.exists("data/bot.sqlite")
Connection = sqlite3.connect("data/bot.sqlite")
Cursor = Connection.cursor()
if dbExists:
    res = Cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='´variables'");
    if res is None:
        dbExists = False

# ToDo: use ID instead of name for ticket category
# ToDo: Add comments


token = os.environ.get("api-token", "")
Ticket_admin_role_id = int(os.environ.get("ticket_admin_role_id", 0))
Ticket_mod_role_id = int(os.environ.get("ticket_mod_role_id", 0))
Guild_id = int(os.environ.get("guild_id", 0))
Open_ticket_channel_id = int(os.environ.get("open_ticket_channel_id", 0))
Tickets_category_name = os.environ.get("tickets_category_name", 0)
Rules_channel_id = os.environ.get("rules_channel_id", 0)
Owner_id = int(os.environ.get("owner_id", 0))
First_join_channel_id = int(os.environ.get("first_join_channel_id", 0))
Neuling_role_id = int(os.environ.get("neuling_role_id", 0))
Bewerber_role_id = int(os.environ.get("bewerber_role_id", 0))
Anmeldung_role_id = int(os.environ.get("anmeldung_role_id", 0))
Anmeldungs_channel_id = int(os.environ.get("anmeldungs_channel", 0))
Bewerber_channel_id = int(os.environ.get("bewerber_channel", 0))
Spam_channel_id = int(os.environ.get("spam_channel_id", 0))
Channel_list = json.loads(os.environ.get("channel_list", "[]"))
Secret = os.environ.get("secret", "")
Form_url = os.environ.get("form_url", "")
API_keys_pterodactyl = json.loads(os.environ.get("api_keys_pterodactyl", "[]"))
Watchdog_channel_id = int(os.environ.get("watchdog_channel_id", 0))
# since the old channels no longer exist and the API-call trying to fetch them won't return, we force the new system:
# Alte_freischaltung = os.environ.get("Alte_freischaltung", "True").lower() in ('true', 't', 1, 'yes', 'y')
Alte_freischaltung = False

Platforms_channel_id = int(os.environ.get("platforms_channel_id", 0))

# Instanz des Bots:
bot = alfred.Alfred()

Open_ticket_message = None
Open_ticket_channel = None
Open_ticket_reaction = '📩'
Ticket_info_reaction = 'ℹ️'
Close_ticket_reaction = '🔒'
Reopen_ticket_reaction = '🔓'
Delete_ticket_reaction = '⛔'
Admin_ticket_reaction = '🛡️'
Self_assign_ticket_reaction = '🔗'
Unassign_ticket_reaction = '✂️'
Accept_reaction = '✅'
Reject_reaction = '❌'
Thumbs_up_reaction = '👍'
Thumbs_down_reaction = '👎'
Microphone_emoji = '🎙'
Pen_ballpoint_emoji = '🖊'
Cityscape_emoji = '🏙'
Crossed_swords_emoji = '⚔️'
Art_emoji = '🎨'
Island_emoji = '🏝'
No_entry_sign_emoji = '🚫'
Apply_reaction_id = int(os.environ.get("apply_reaction_id", 0))
Apply_reaction = None # initialized later
Ticket_message_reactions = [Close_ticket_reaction, Admin_ticket_reaction, Self_assign_ticket_reaction,
                            Unassign_ticket_reaction, Ticket_info_reaction]
Close_ticket_message_reactions = [Accept_reaction, Reject_reaction, Close_ticket_reaction]
Reopen_ticket_message_reactions = [Reopen_ticket_reaction, Delete_ticket_reaction, Ticket_info_reaction]
Delete_ticket_message_reactions = [Accept_reaction, Reject_reaction, Delete_ticket_reaction]
Poll_message_reactions = [Thumbs_up_reaction, Thumbs_down_reaction]
Rules_message_reactions = [Microphone_emoji, Pen_ballpoint_emoji, Cityscape_emoji, Crossed_swords_emoji, Art_emoji,
                           Island_emoji, No_entry_sign_emoji]
Ticket_category = None
Guild = None
Ticket_admin_role = None
Ticket_mod_role = None
Ticket_id = None
Owner = None
last_opened_ticket = []
announcement_colour = []
First_join_channel = None
First_join_channel_reaction_message = None
Anmeldung_category = None
Neuling_role = None
Bewerber_role = None
Anmeldung_role = None
TechAdmin_role = None
Rules_channel = None
Anmeldungs_channel = None
Bewerber_channel = None
Spam_channel = None
Rules_messages = []
vc = None
Platforms_channel = None
Platforms = {
    'java': ['Java-Version am PC', int(os.environ.get("platform_role_java_id", 0))],
    'bedrock': ['Bedrock-Version am PC', int(os.environ.get("platform_role_bedrock_id", 0))],
    'playstation': ['Playstation', int(os.environ.get("platform_role_playstation_id", 0))],
    'xbox': ['X-Box', int(os.environ.get("platform_role_xbox_id", 0))],
    'switch': ['Switch', int(os.environ.get("platform_role_switch_id", 0))],
    'status': ['Auswahl anzeigen', None],
}


""" das will so nicht, wirft Fehler
# special permissions in a more readable way
# allow permissions: 523328 -> Bits 6, 10-18 wobei 12 und 13 eigentlich nicht gesetzt sein sollten
Private_allow_permissions = discord.Permissions.add_reactions() + discord.Permissions.view_channel() +        \
                            discord.Permissions.send_messages() + discord.Permissions.embed_links() +         \
                            discord.Permissions.attach_files() + discord.Permissions.read_message_history() + \
                            discord.Permissions.mention_everyone() + discord.Permissions.external_emojis()
# deny permissions: 12288 -> Bits 12, 13
Private_deny_permissions = discord.Permissions.send_tts_messages() + discord.Permissions.manage_messages()
"""
Private_allow_permissions = 523328
Private_deny_permissions = 12288
