import discord

import config

from platformroles import platformroles
from utils import views

class SelectButton(discord.ui.Button):
    def __init__(self, platform, label, role, id):
        super().__init__(label=label,style=SelectButton.get_style(role), custom_id=id)
        self.platform = platform
    
    def get_style(role):
        if role == None:
            return discord.ButtonStyle.success
        else:
            return discord.ButtonStyle.blurple

    async def callback(self,interaction):
        await handle_platform_select(interaction, self.platform)


class PlatformSelectView(discord.ui.View):
    def __init__(self):
        super().__init__(timeout=None)
        self.embed = discord.Embed(title="Auswahl der Spieleplattform(en)",
                            description="Bitte wähle hier aus, auf welchem Gerät bzw. welchen Geräten du spielst.",
                                color=0x33dd33)
        for pf in config.Platforms:
            self.add_item(SelectButton(pf, config.Platforms[pf][0], config.Platforms[pf][1], 'platform_select_button_' + pf))
            

    async def show(self, ch):
        self.message = await ch.send(view=self, embed=self.embed)
    
__PlatformSelect = None

#Initialisierung durch dern Bot
def get_view(): 
    global __PlatformSelect
    
    __PlatformSelect = PlatformSelectView()
    return __PlatformSelect

async def create_message(ctx):
    global __PlatformSelect
    
    await ctx.message.delete()
    # muss in richtigem Kanal sein
    if ctx.channel != config.Platforms_channel:
        await ctx.channel.send("Das ist nur im Plattformen-Kanal möglich!")
        return
    # wir gehen davon aus, dass die Kanalberechtigungen stimmen und nur berechtigte den Befehl absetzen können
    # alles übrige im Kanal entfernen
    await ctx.channel.purge()
    # und das View zeigen:
    await __PlatformSelect.show(ctx.channel)
    
async def handle_platform_select(interaction, platform):
    pData = config.Platforms[platform]
    button_role  = pData[1]
    user  = interaction.user
    select_text = ''
    # user.roles aktualisiert sich NICHT!
    if button_role != None:
        if button_role in user.roles:
            await user.remove_roles(button_role)
            action = 'entfernt'
            add = False
        else:
            await user.add_roles(button_role)
            action = 'zugewiesen'
            add = True
        select_text = 'Plattform **' + pData[0] + '** wurde ' + action + '.\n\n'
    has_role = False
    text = '__Deine ausgewählten Plattformen:__ '
    for pf in config.Platforms:
        pData = config.Platforms[pf]
        role  = pData[1]
        if role != None:
            # da button_role falsch in user.roles steht, dieses gesondert behandeln
            if role == button_role:
                show = add
            else:
                show = role in user.roles
            if show:
                text += '\n- ' + pData[0]
                has_role = True
    if not has_role:
        text = 'Du hast noch keine Plattform ausgewählt.'
    await interaction.response.send_message(content=select_text + text, ephemeral=True, delete_after=60)
