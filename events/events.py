import asyncio

import discord
from datetime import datetime, timedelta
import threading
from ticketsystem import ticketsystem
from anmeldung import anmeldung
from platformroles import platformroles

import config
from utils import utils

# ToDo move Ticket-parts to new module
# ToDo: Add comments
# ToDo: implement ToDo-List. https://discordpy.readthedocs.io/en/latest/api.html#discord.Client.wait_for

@config.bot.event
async def on_message(message):
    if message.author == config.bot.user:
        return
    if message.channel.type not in [discord.ChannelType.group, discord.ChannelType.private]:
        if message.channel.category == config.Ticket_category:
            for x in config.last_opened_ticket:
                if x[0] == message.author.id and x[2] == message.channel.id:
                    config.last_opened_ticket.remove([x[0], x[1], x[2]])
                    break

    if message.content.lower().startswith('hallo alfred'):
        await message.channel.send('Hallo ' + message.author.name + '!')
        await utils.owner_relay(message)
        return
    elif not message.guild:
        if message.content.lower().startswith('danke'):
            await message.channel.send('Ich mach nur meinen Job!')
            await utils.owner_relay(message)
            return
        else:
            await message.author.send("Leider darf ich dir keine Direknachrichten beantworten")
            await utils.owner_relay(message)
            return

    await config.bot.process_commands(message)


@config.bot.event
async def on_ready():
    discord.utils.setup_logging()
    print('We have logged in as {0}'.format(config.bot.user))
    # Bot initialisieren:
    config.bot.initialize()
    # Rollen, Channels, etc. holen
    config.Guild = config.bot.get_guild(config.Guild_id)
    roles = await config.Guild.fetch_roles()
    for role in roles:
        if role.id == config.Ticket_admin_role_id:
            config.Ticket_admin_role = role
        elif role.id == config.Ticket_mod_role_id:
            config.Ticket_mod_role = role
    config.Open_ticket_channel = await config.bot.fetch_channel(config.Open_ticket_channel_id)
    config.First_join_channel = await config.bot.fetch_channel(config.First_join_channel_id)
    config.Anmeldung_category = config.First_join_channel.category
    config.Neuling_role = config.Guild.get_role(config.Neuling_role_id)
    config.Bewerber_role = config.Guild.get_role(config.Bewerber_role_id)
    config.Anmeldung_role = config.Guild.get_role(config.Anmeldung_role_id)
    config.Rules_channel = await config.bot.fetch_channel(config.Rules_channel_id)
    # the following two channels no longer exist, and aren't used anyway _as long as config.Alte_freischaltung is False_ (see config.py)
    # and the API call won't return in that case, so skip it
    # config.Anmeldungs_channel = await config.bot.fetch_channel(config.Anmeldungs_channel_id)
    # config.Bewerber_channel = await config.bot.fetch_channel(config.Bewerber_channel_id)
    config.Spam_channel = await config.bot.fetch_channel(config.Spam_channel_id)
    config.Platforms_channel = await config.bot.fetch_channel(config.Platforms_channel_id)
    #config-Rollen festlegen
    for key in config.Platforms:
        if config.Platforms[key][1] != None: # nicht für Abfrage
            config.Platforms[key][1] = config.Guild.get_role(config.Platforms[key][1])
            config.Platforms[key][0] = config.Platforms[key][1].name
            
    config.Cursor.execute("SELECT * FROM variables")
    data = config.Cursor.fetchall()  # Possible because string in column variable is unique as ist the primary key
    for x in data:
        print(f'- processing variable {x[0]} -> {x[1]}')
        if x[0] == 'Open_ticket_message':
            config.Open_ticket_message = await config.Open_ticket_channel.fetch_message(x[1])
            config.Ticket_category = discord.utils.get(config.Open_ticket_message.guild.categories,
                                                       name=config.Tickets_category_name)

        elif x[0] == 'Ticket_id':
            config.Ticket_id = x[1]

        elif False and x[0] == "First_join_channel_reaction_message": # now done below
            try:
                config.First_join_channel_reaction_message = await config.First_join_channel.fetch_message(x[1])
            except discord.errors.NotFound:
                print("first join message not found, please renew!")
    # first_join_message is the first/only in first_join_channel
    if config.First_join_channel_reaction_message == None:
        async for msg in config.First_join_channel.history(limit=2):
            if config.First_join_channel_reaction_message == None:
                config.First_join_channel_reaction_message = msg
            else:
                print("ERROR: two or more messages found in the newbie channel, please use ~cnrm there!")
        if config.First_join_channel_reaction_message == None:
            print("ERROR: no messages found in newbie channel, please use ~cnrm there!")
    
                
    if not config.Ticket_id:
        config.Ticket_id = 1

    await utils.send_rules_message()

    activity = discord.Game("mit den Tickets")
    await config.bot.change_presence(status=discord.Status.online, activity=activity)
    config.Owner = await config.bot.fetch_user(config.Owner_id)

    config.Apply_reaction = await config.Guild.fetch_emoji(config.Apply_reaction_id)
    await config.Spam_channel.send("Ich wurde neu gestartet und bin jetzt einsatzbereit!")

@config.bot.event
async def on_raw_reaction_add(payload):
    user = payload.member
    if user == config.bot.user:  # Reaction was added by the Bot. Do nothing
        return
    channel = await config.bot.fetch_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)
    if message.author != config.bot.user:  # reaction wasn't added to a message from the bot. Do nothing
        return

    # await asyncio.sleep(5)

    if channel.category == config.Ticket_category:
        print(f'processing reaction in Ticket')
        await ticketsystem.do_ticket_reaction(payload)
    elif anmeldung.is_anmeldung_message(message):
        print(f'processing reaction in Anmeldung')
        await message.remove_reaction(payload.emoji, user)
        if payload.emoji.name == config.Accept_reaction:
            await anmeldung.do_anmeldung(user, payload)
        elif payload.emoji.id == config.Apply_reaction_id:
            await anmeldung.do_bewerbung(user, payload)
    elif channel == config.Rules_channel:
        print(f'processing reaction in Rules')
        if utils.is_bot_message(message, config.Reject_reaction):
            for rules_message in config.Rules_messages:
                if rules_message[0] == message:
                    config.Rules_messages.remove(rules_message)
                    await rules_message[0].delete()
                    await rules_message[2].delete()
                    return
        elif utils.is_bot_message(message, config.Rules_message_reactions):
            if payload.emoji.name == config.Microphone_emoji:
                embed = discord.Embed(title="Discord Regelwerk",
                                      description="<https://minecraft-kids.online/discord-regelwerk/>",
                                      color=0xaaaaaa)
            elif payload.emoji.name == config.Pen_ballpoint_emoji:
                embed = discord.Embed(title="Nutzungsbedingungen",
                                      description="<https://minecraft-kids.online/nutzungsbedingungen-kinder-server/>",
                                      color=0xaaaaaa)
            elif payload.emoji.name == config.Cityscape_emoji:
                embed = discord.Embed(title="Regelwerk Plot / Citybuild",
                                      description="<https://minecraft-kids.online/regelwerk-plot-server/>",
                                      color=0xaaaaaa)
            elif payload.emoji.name == config.Crossed_swords_emoji:
                embed = discord.Embed(title="Regelwerk Survival",
                                      description="<https://minecraft-kids.online/regelwerk-survival-server/>",
                                      color=0xaaaaaa)
            elif payload.emoji.name == config.Art_emoji:
                embed = discord.Embed(title="Regelwerk Kreativ und Redworld",
                                      description="<https://minecraft-kids.online/regelwerk-kreativ/redworld-server/>",
                                      color=0xaaaaaa)
            elif payload.emoji.name == config.Island_emoji:
                embed = discord.Embed(title="Regelwerk Skyblock",
                                      description="Für Skyblock ist noch kein eigenes Regelwerk verfasst, es gelten prinzipiell aber dieselben allgemeinen Regeln wie auf den anderen Servern.",
                                      color=0xaaaaaa)
            elif payload.emoji.name == config.No_entry_sign_emoji:
                embed = discord.Embed(title="Verbannungen / Stumm / Verwarnungen",
                                      description="<https://banliste.minecraft-kids.online/index.php>",
                                      color=0xaaaaaa)
            else:
                embed = discord.Embed(title="Fehler",
                                      description="Fehler",
                                      color=0xaaaaaa)
            temp_message_mention = await channel.send("{.mention}".format(user))
            temp_message = await config.Rules_channel.send(embed=embed)
            await temp_message.add_reaction(config.Reject_reaction)
            config.Rules_messages.append([temp_message, datetime.now(), temp_message_mention])

        await message.remove_reaction(payload.emoji, user)
    elif channel == config.Platforms_channel:
        print(f'processing reaction in Platforms')
        await message.remove_reaction(payload.emoji, user)
        await platformroles.handle(payload)
    else:
        print(f'cannot process reaction in unrecognized channel {channel}')
        pass


@config.bot.event
async def on_raw_reaction_remove(payload):
    user = payload.member
    if user == config.bot.user:  # Reaction was added by the Bot. Do nothing
        return

    channel = await config.bot.fetch_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)
    if message.author != config.bot.user:  # reaction wasn't added to a message from the bot. Do nothing
        return

    # await asyncio.sleep(5)

    if channel.category == config.Ticket_category:

        if utils.is_ticket_message(message):

            if payload.emoji.name == config.Admin_ticket_reaction:
                if any(x.id in [config.Ticket_admin_role_id, config.Ticket_mod_role_id] for x in user.roles):
                    await channel.set_permissions(config.Ticket_mod_role,
                                                  overwrite=discord.PermissionOverwrite.from_pair(
                                                      discord.Permissions.text(), discord.Permissions.none()))
                    utils.update_ticket_db(channel.id, admin_mode=0)
                    await channel.send("Der Adminmodus wurde abgeschaltet")

# @bot.event
# async def on_voice_state_update(member, before, after):
#     if member == bot.user:
#         return
#
#     if after.channel is not None:
#         if after.channel.channel_id == 799322607535128586:
#             await member.voice.channel.connect()
#
#     if before.channel is not None:
#         if before.channel.channel_id == 799322607535128586:
#             if len(before.channel.members) == 1 and before.channel.members[0].channel_id is bot.user.channel_id:
#                 for vc in bot.voice_clients:
#                     if vc.guild.channel_id == Guild_id:
#                         await vc.disconnect()
#             else:
#                 print(before.channel.members)
