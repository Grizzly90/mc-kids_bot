

import discord
from mcstatus import JavaServer

import config


async def show_status(ctx):
    print("Getting server status ...")
    server = JavaServer.lookup("spiel.minecraft-kids.online:25577")
    status = server.status()
    print(f"The server has {status.players.online} player(s) online and replied in {status.latency} ms")

    latency = server.ping()
    print(f"The server replied in {latency} ms")

    query = server.query()
    print(f"The server has the following players online: {', '.join(query.players.names)}")
    channel = await config.bot.fetch_channel(payload.channel_id)
    await channel.send(f"The server has the following {status.players.online} players online: {', '.join(query.players.names)}")
